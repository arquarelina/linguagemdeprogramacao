object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 200
  Width = 515
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\Users\Informatica 03\Documents\EasyPHP-DevServer-14.1VC9\bina' +
      'ries\mysql\lib\libmysql.dll'
    Left = 56
    Top = 32
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokedex'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 160
    Top = 32
  end
  object FDQueryPokemon: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from  pokemon')
    Left = 272
    Top = 32
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 376
    Top = 32
  end
  object FDQueryTreinador: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from treinador')
    Left = 272
    Top = 104
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 368
    Top = 104
  end
end
